package tictactoe.v6

import scala.scalajs.js.annotation.{JSExport, ScalaJSDefined}

import sri.core.ReactComponent
import sri.web.all._
import sri.web.vdom.htmltags._


sealed trait Piece
case object X extends Piece
case object O extends Piece

sealed trait MaybePiece
case object NoPiece extends MaybePiece
case class YesPiece(piece: Piece) extends MaybePiece


object Board {
  val size = 3
}

import Board.size


// The board is represented with a single index as shown:
//  0 | 1 | 2
// ---+---+---
//  3 | 4 | 5
// ---+---+---
//  6 | 7 | 8
case class State(board: Array[MaybePiece] = Array.fill(size * size)(NoPiece),
                 piece: Piece = X)

@ScalaJSDefined
@JSExport
class Game extends ReactComponent[Unit, State] {
  override def componentWillMount() = setState(State())

  override def render() = {
    val doMove = (i: Int) =>
      setState(
        State(
          state.board.updated(i, YesPiece(state.piece)),
          if (state.piece == X) O else X
        )
      )

    val winner = () => {
      case class LineSpec(start: Int, skip: Int) {
        val indexes = Array.iterate(start, size)(index => index + skip)
      }
      val horizontal = (start: Int) => LineSpec(start, 1)
      val vertical = (start: Int) => LineSpec(start, size)
      val diagonal = (start: Int) => LineSpec(start, size + 1)
      val antidiagonal = (start: Int) => LineSpec(start, size - 1)

      val lineSpecs =
        Range(0, size * size, size).map(horizontal) ++
          Range(0, size).map(vertical) ++
          Array(diagonal(0), antidiagonal(size - 1))

      val hasLine =
        (piece: Piece) =>
          lineSpecs
            .exists(line =>
              line.indexes.forall(i => state.board(i) == YesPiece(piece))
            )

      if (hasLine(X)) YesPiece(X)
      else if (hasLine(O)) YesPiece(O)
      else NoPiece
    }

    div()(
      h1()("Turn: ", state.piece.toString),
      h2()(
        "Winner: ",
        winner() match {
          case NoPiece => "No one, yet..."
          case YesPiece(piece) => piece.toString
        }
      ),
      table()(
        tbody()(
          Range(0, size).map(row =>
            tr(key = row)(
              Range(0, size).map(col => {
                val i = row * size + col
                td(key = col, onClick = (_: ReactEventH) => doMove(i))(
                  state.board(i) match {
                    case NoPiece         => ""
                    case YesPiece(piece) => piece.toString
                  }
                )
              })
            )
          )
        )
      )
    )
  }
}
