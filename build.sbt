name := "qbmeetup-tictactoe"
version := "1.0"
scalaVersion := "2.11.8"

enablePlugins(ScalaJSPlugin, WorkbenchPlugin)
localUrl := "localhost" -> 3210
scalaJSModuleKind := ModuleKind.NoModule

resolvers += Resolver.jcenterRepo
libraryDependencies += "io.github.nafg" %%% "sri-web" % "0.6.0-M1"
jsDependencies ++= Seq(
  ("org.webjars.bower" % "react" % "15.3.1" / "react-with-addons.js")
    .minified("react-with-addons.min.js"),
  ("org.webjars.bower" % "react" % "15.3.1" / "react-dom.js")
    .minified("react-dom.min.js")
    .dependsOn("react-with-addons.js"),
  ("org.webjars.npm" % "history" % "3.0.0-2" / "history.js")
    .minified("history.min.js")
)
