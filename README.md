Run

```
sbt ~fastOptJS
```

and point your browser to http://localhost:3210/index.html

When you change the code and save, it will recompile and reload the page
automatically.
