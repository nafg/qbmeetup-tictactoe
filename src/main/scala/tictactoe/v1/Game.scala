package tictactoe.v1

import scala.scalajs.js.annotation.{JSExport, ScalaJSDefined}

import sri.core.ReactComponent
import sri.web.all._
import sri.web.vdom.htmltags._


// The board is represented with a single index as shown:
//  0 | 1 | 2
// ---+---+---
//  3 | 4 | 5
// ---+---+---
//  6 | 7 | 8
case class State(board: Array[String] = Array.fill(9)(""),
                 piece: String = "X")


@ScalaJSDefined
@JSExport
class Game extends ReactComponent[Unit, State] {
  override def componentWillMount() = setState(State())

  override def render() = {
    var doMove = (i: Int) =>
      setState(
        State(
          state.board.updated(i, state.piece),
          if (state.piece == "X") "O" else "X"
        )
      )
    var winner = () => {
      /**
        * Describes "win" lines as a pair of (1) a starting index,
        * and (2) the number of indexes to skip between the three spots.
        * Skip of 1 = horizontal (adjacent spots),
        * 2 = down and left (one short of straight down)
        * 3 = vertical (the size of a row)
        * 4 = down and right (one more than straight down)
        */
      var lineSpecs = Array((0, 1), (3, 1), (6, 1), (0, 3), (1, 3), (2, 3), (0, 4), (2, 2))
      /**
        * Materializes a "line spec" into an array of the line's indexes
        */
      var makeLine = (lineSpec: (Int, Int)) => {
        var index = lineSpec._1
        var skip = lineSpec._2
        var counter = 0
        var len = 3
        var array = new Array[Int](len)
        while (counter < len) {
          array(counter) = index
          index += skip
          counter += 1
        }
        array
      }

      var hasLine =
        (piece: String) =>
          lineSpecs
            .map(makeLine)
            .exists(line =>
              line.forall(i => state.board(i) == piece)
            )

      if (hasLine("X")) "X"
      else if (hasLine("O")) "O"
      else ""
    }

    div()(
      h1()("Turn: ", state.piece),
      h2()("Winner: ", winner()),
      table()(
        tbody()(
          Range(0, 3).map(row =>
            tr(key = row)(
              Range(0, 3).map(col => {
                var i = row * 3 + col
                td(key = col, onClick = (e: ReactEventH) => doMove(i))(
                  state.board(i)
                )
              })
            )
          )
        )
      )
    )
  }
}
