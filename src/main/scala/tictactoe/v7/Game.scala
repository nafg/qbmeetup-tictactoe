package tictactoe.v7

import scala.scalajs.js.annotation.{JSExport, ScalaJSDefined}

import sri.core.ReactComponent
import sri.web.all._
import sri.web.vdom.htmltags._


sealed trait Piece
case object X extends Piece
case object O extends Piece

sealed trait MaybePiece
case object NoPiece extends MaybePiece
case class YesPiece(piece: Piece) extends MaybePiece


object Board {
  val size = 3
}
class Board(pieces: Array[MaybePiece] = Array.fill(Board.size * Board.size)(NoPiece)) {

  import Board.size


  val coordToIndex = (col: Int, row: Int) => row * size + col

  val get = (col: Int, row: Int) => pieces(coordToIndex(col, row))

  val updated = (col: Int, row: Int, piece: Piece) =>
    new Board(pieces.updated(coordToIndex(col, row), YesPiece(piece)))

  val winner = () => {
    case class LineSpec(start: Int, skip: Int) {
      val indexes = Array.iterate(start, size)(index => index + skip)
    }
    val horizontal = (start: Int) => LineSpec(start, 1)
    val vertical = (start: Int) => LineSpec(start, size)
    val diagonal = (start: Int) => LineSpec(start, size + 1)
    val antidiagonal = (start: Int) => LineSpec(start, size - 1)

    val lineSpecs =
      Range(0, size * size, size).map(horizontal) ++
        Range(0, size).map(vertical) ++
        Array(diagonal(0), antidiagonal(size - 1))

    val hasLine =
      (piece: Piece) =>
        lineSpecs
          .exists(line =>
            line.indexes.forall(i => pieces(i) == YesPiece(piece))
          )

    if (hasLine(X)) YesPiece(X)
    else if (hasLine(O)) YesPiece(O)
    else NoPiece
  }

  val cols = Range(0, size)
  val rows = Range(0, size)
}


case class State(board: Board = new Board, piece: Piece = X)

@ScalaJSDefined
@JSExport
class Game extends ReactComponent[Unit, State] {
  override def componentWillMount() = setState(State())

  override def render() = {
    val doMove = (col: Int, row: Int) =>
      setState(
        State(
          state.board.updated(col, row, state.piece),
          if (state.piece == X) O else X
        )
      )

    div()(
      h1()("Turn: ", state.piece.toString),
      h2()(
        "Winner: ",
        state.board.winner() match {
          case NoPiece         => "No one, yet..."
          case YesPiece(piece) => piece.toString
        }
      ),
      table()(
        tbody()(
          state.board.rows.map(row =>
            tr(key = row)(
              state.board.cols.map(col =>
                td(key = col, onClick = (_: ReactEventH) => doMove(col, row))(
                  state.board.get(col, row) match {
                    case NoPiece         => ""
                    case YesPiece(piece) => piece.toString
                  }
                )
              )
            )
          )
        )
      )
    )
  }
}
