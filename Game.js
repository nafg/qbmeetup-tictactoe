const e = name => props => (...children) => React.createElement(name, props, ...children);
const div = e('div'), h1 = e('h1'), h2 = e('h2'), table = e('table'), tbody = e('tbody'), tr = e('tr'), td = e('td');

Array.prototype.updated = function (index, value) {
  const a2 = this.slice();
  a2[index] = value;
  return a2;
};

const Range = (a, b) => (a >= b) ? [] : [a, ...Range(a + 1, b)];

// The board is represented with a single index as shown:
//  0 | 1 | 2
// ---+---+---
//  3 | 4 | 5
// ---+---+---
//  6 | 7 | 8
function State(board = new Array(9).fill(""),
               piece = "X") {
  return {board, piece};
}

class Game extends React.Component {
  componentWillMount() {
    this.setState(State())
  }

  render() {
    const state = this.state;
    var doMove = (i) =>
        this.setState(
            State(
                state.board.updated(i, state.piece),
                (state.piece === "X") ? "O" : "X"
            )
        );
    var winner = () => {
      /**
       * Describes "win" lines as a pair of (1) a starting index,
       * and (2) the number of indexes to skip between the three spots.
       * Skip of 1 = horizontal (adjacent spots),
       * 2 = down and left (one short of straight down)
       * 3 = vertical (the size of a row)
       * 4 = down and right (one more than straight down)
       */
      var lineSpecs = [[0, 1], [3, 1], [6, 1], [0, 3], [1, 3], [2, 3], [0, 4], [2, 2]];
      /**
       * Materializes a "line spec" into an array of the line's indexes
       */
      var makeLine = (lineSpec) => {
        var index = lineSpec[0];
        var skip = lineSpec[1];
        var counter = 0;
        var len = 3;
        var array = new Array(len);
        while (counter < len) {
          array[counter] = index;
          index += skip;
          counter += 1
        }
        return array
      };

      var hasLine =
          (piece) =>
              lineSpecs
                  .map(makeLine)
                  .some(line =>
                      line.every(i => state.board[i] === piece)
                  );

      if (hasLine("X")) return "X";
      else if (hasLine("O")) return "O";
      else return ""
    };

    return div()(
        h1()("Turn: ", state.piece),
        h2()("Winner: ", winner()),
        table()(
            tbody()(
                Range(0, 3).map(row =>
                    tr({key: row})(
                        Range(0, 3).map(col => {
                          var i = row * 3 + col;
                          return td({key: col, onClick: (e) => doMove(i)})(
                              state.board[i]
                          )
                        })
                    )
                )
            )
        )
    )
  }
}

if (!window.tictactoe) window.tictactoe = {};
window.tictactoe.v0 = {Game};
