package tictactoe.v9

import scala.scalajs.js.annotation.{JSExport, ScalaJSDefined}

import sri.core.ReactComponent
import sri.web.all._
import sri.web.vdom.htmltags._


sealed trait Piece
case object X extends Piece
case object O extends Piece

sealed trait Maybe[T]
case class No[T]() extends Maybe[T]
case class Yes[T](value: T) extends Maybe[T]


abstract class AbstractBoard {
  type Col
  type Row
  val get: (Col, Row) => Maybe[Piece]
  val updated: (Col, Row, Piece) => Maybe[AbstractBoard]
  val winner: () => Maybe[Piece]
  val cols: Iterable[Col]
  val rows: Iterable[Row]
}

object Board {
  val size = 3
}
class Board(pieces: Array[Maybe[Piece]] = Array.fill(Board.size * Board.size)(No())) extends AbstractBoard {

  import Board.size


  type Col = Int
  type Row = Int

  val coordToIndex = (col: Int, row: Int) => row * size + col

  val get = (col: Int, row: Int) => pieces(coordToIndex(col, row))

  val updated = (col: Int, row: Int, piece: Piece) =>
    get(col, row) match {
      case No()          => Yes[AbstractBoard](new Board(pieces.updated(coordToIndex(col, row), Yes(piece))))
      case Yes(whatever) => No[AbstractBoard]()
    }

  val winner = () => {
    case class LineSpec(start: Int, skip: Int) {
      val indexes = Array.iterate(start, size)(index => index + skip)
    }
    val horizontal = (start: Int) => LineSpec(start, 1)
    val vertical = (start: Int) => LineSpec(start, size)
    val diagonal = (start: Int) => LineSpec(start, size + 1)
    val antidiagonal = (start: Int) => LineSpec(start, size - 1)

    val lineSpecs =
      Range(0, size * size, size).map(horizontal) ++
        Range(0, size).map(vertical) ++
        Array(diagonal(0), antidiagonal(size - 1))

    val hasLine =
      (piece: Piece) =>
        lineSpecs
          .exists(line =>
            line.indexes.forall(i => pieces(i) == Yes(piece))
          )

    if (hasLine(X)) Yes[Piece](X)
    else if (hasLine(O)) Yes[Piece](O)
    else No[Piece]()
  }

  val cols = Range(0, size)
  val rows = Range(0, size)
}


case class State(board: AbstractBoard = new Board, piece: Piece = X)

@ScalaJSDefined
@JSExport
class Game extends ReactComponent[Unit, State] {
  override def componentWillMount() = setState(State())

  override def render() = {
    val board = state.board
    val doMove = (col: board.Col, row: board.Row) =>
      board.updated(col, row, state.piece) match {
        case Yes(newBoard) => setState(State(newBoard, if (state.piece == X) O else X))
        case No()          =>
      }

    div()(
      h1()("Turn: ", state.piece.toString),
      h2()(
        "Winner: ",
        board.winner() match {
          case No()       => "No one, yet..."
          case Yes(piece) => piece.toString
        }
      ),
      table()(
        tbody()(
          board.rows.map(row =>
            tr(key = row.toString)(
              board.cols.map(col =>
                td(key = col.toString, onClick = (_: ReactEventH) => doMove(col, row))(
                  board.get(col, row) match {
                    case No()       => ""
                    case Yes(piece) => piece.toString
                  }
                )
              )
            )
          )
        )
      )
    )
  }
}
